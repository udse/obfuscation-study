trimExclude                * and
                           *.* and
                           *.* * and
                           *.* *(*)
                           ;
obfuscate   keepGenericsInfo=false
            keepInnerClassInfo=false
            obfuscateFlow=none     // can change from none to light, normal or aggressive
            encryptStringLiterals=none  // can change from none to normal, aggressive or flowObfuscate
            exceptionObfuscation=none  // can change from none to light or heavy
            autoReflectionHandling=none // can change from none to normal
            randomize=false
            preverify=false
            mixedCaseClassNames=false
            ;

