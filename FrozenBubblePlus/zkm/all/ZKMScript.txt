obfuscate   keepGenericsInfo            = false
            keepInnerClassInfo          = false
            obfuscateFlow               = normal //can change from none to light, normal or aggressive
            encryptStringLiterals       = normal //can change from none to normal, aggressive or flowObfuscate
            exceptionObfuscation        = none //can change from none to light or heavy
            autoReflectionHandling      = none //can change from none to normal
            randomize                   = false //can change from false to true
            preverify                   = false //don't need to preverify if only used for Android
            mixedCaseClassNames         = false
            ;

