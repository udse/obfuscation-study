Copy local.properties.sample to local.properies.  Update the sdk and ndk locations to point to your local copies.

To enable a specific obfuscation tool, copy <tool>.properites.sample to <tool>.properites and update the contents to point to you local copy of the tool.

Each application is placed in its own directory.

Within each application directory, there are subdirectories, one for each obfuscation tool and one for the original unobfuscated version.  Within each tool directory is a subdirectory for each obfuscation configuration.

To build all application versions:
    find . -name 'build.xml' ! -wholename '*/libraries/*' -execdir ant release \;

To clean all applications:
    find . -name 'build.xml' ! -wholename '*/libraries/*' -execdir ant clean \;