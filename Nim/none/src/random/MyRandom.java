package random;

import java.util.Random;

public class MyRandom {
	
	public static MyRandom myrandom = new MyRandom();
	
	Random gen;
	
	public MyRandom() {
		gen = new Random(123456);
	}
	
	public double random() {
		return gen.nextDouble();
	}
	
}
